import numpy as np
import activation_functions
from scipy import optimize

class MLP :
    def __init__(self, inUnits=4, hiddenUnits=6, outUnits=3, activation=None, seed=None, bias=False, weightInit='random', debug=False) :
        self.bias=False
        self.debug = debug
        
        if bias == True :
            hiddenUnits += 1
            inUnits += 1
            self.bias=True
            
        self.l1_ = inUnits
        self.l2_ = hiddenUnits
        self.l3_ = outUnits
        
#        if bias == True :
#            w1_ = (self.l1_, self.l2_-1)
#            w2_ = (self.l2_, self.l3_)
#        else :
        w1_ = (self.l1_, self.l2_)
        w2_ = (self.l2_, self.l3_)
            
        self.debug and print("w1_ = %s, w2_ =%s" % (w1_, w2_) )
        
        act = activation_functions.Activation()
        
        if activation==None or activation=='sigmoid' :
            af = act.sigmoid
            afPrime = act.sigmoidPrime
        elif activation=='tanh' : 
            af = act.tanh
            afPrime = act.tanhPrime            
        elif activation=='sigmoidSpecial' : 
            af = act.sigmoidSpecial
            afPrime = act.sigmoidSpecialPrime
        elif activation=='none' : 
            af = act.none
            afPrime = act.none
            
        self.af = af
        self.afPrime = afPrime
        
        if seed is not None :
            np.random.seed(seed)
            
        if weightInit=='zeros':
            self.w1_ = np.zeros(w1_)
            self.w2_ = np.zeros(w2_)
        elif weightInit=='random' or weightInit=='normal' or weightInit=='gaussian':
            self.w1_ = np.random.randn(*w1_)
            self.w2_ = np.random.randn(*w2_)
        elif weightInit=='uniform':
            self.w1_ = np.random.uniform(size=w1_)
            self.w2_ = np.random.uniform(size=w2_)
            
        #reset the seed
        np.random.seed()
        
    def __str__ (self) :        
        return "NN (%d, %d, %d) \
            w1 : %s \
            w2 : %s \
            bias : %s " %  \
            (self.l1_, self.l2_, self.l3_, \
             np.array(self.w1_), np.array(self.w2_),
             self.bias,)

        
    def propagate(self, X) :
        if self.bias == True and X.shape[1] != self.l1_:
            self.debug and print("PROPAGATE: Adding bias: X = %s" % (X,))
            X = np.insert(X, obj=[0], values=[1], axis=1)
        self.z2_ = np.dot(X, self.w1_)
        self.a2_ = self.activation(self.z2_)
        self.debug and print("PROPAGATE 1->2: X = %s, w1 = %s" % (X, self.w1_))
        self.debug and print("PROPAGATE 1->2: z2 = %s" % (self.z2_))
        if self.bias == True :
            #self.a2_ = np.insert(self.a2_, obj=[0], values=[1], axis=1)
            # this is how we do bias
            for item in self.a2_ :
                item[0] = 1
        self.debug and print("PROPAGATE 1->2: a2 = %s, w2 = %s" % (self.a2_, self.w2_))
        self.z3_ = np.dot(self.a2_, self.w2_)
        self.a3_ = self.activation(self.z3_)
        self.debug and print("PROPAGATE 2->3: z3 = %s, a3 = %s" % (self.z2_, self.a3_))
        
        return self.a3_
        
    def activation(self, val) :
        return self.af(val)
    
    def activationPrime(self, val) :
        return self.afPrime(val);
    
    def cost(self, X, expected) :
        self.propagate(X)
        return np.sum(0.5 * (expected - self.a3_)**2)
    
    def costPrime(self, X, expected) :
        originalX = X
        
        if self.bias == True and X.shape[1] != self.l1_:
            self.debug and print("costPrime: adding bias: X = %s" % (X,))
            X = np.insert(X, obj=[0], values=[1], axis=1)
        
        self.debug and print("costPrime: originalX : %s" %  (originalX.shape, ))
        self.debug and print("costPrime: X : %s" %  (X.shape, ))
        
        actual = self.propagate(X)
        
        # output layer error
        error3 = np.multiply(-(expected - self.a3_), self.activationPrime(self.z3_))
        self.debug and print("costPrime: Error3 - %s %s" %  (error3.shape, error3))
        g3 = np.dot(self.a2_.T, error3)
        self.debug and print("costPrime: g3 - %s %s" %  (g3.shape, g3))
        
        # hidden layer error
        error2 = np.dot(error3, self.w2_.T) * self.activationPrime(self.z2_)
        self.debug and print("costPrime: Error2 - %s %s" % (error2.shape, error2))
        #error2 = np.dot(error3, self.w2_.T) * self.a2_ #self.activationPrime(self.z2_)
        g2 = np.dot(X.T, error2)
        self.debug and print("costPrime: g2 - %s %s" %  (g2.shape, g2))
        
        return g2, g3
    
    
    def computeGradients(self, X, y):
        grad1, grad2 = self.costPrime(X, y)
        return np.concatenate((grad1.ravel(), grad2.ravel()))
    
    def getParams(self):
        params = np.concatenate((self.w1_.ravel(), self.w2_.ravel()))
        return params
    
    def setParams(self, params):
        #if self.bias == True :
#            w1_ = (self.l1_, self.l2_-1)
#            w2_ = (self.l2_, self.l3_)
#        else :
        w1_ = (self.l1_, self.l2_)
        w2_ = (self.l2_, self.l3_)
        
        self.debug and print("setParams: params = %s " % (params, ) )
        #Set W1 and W2 using single paramater vector.
        W1_start = 0
        #W1_end = self.l2_ * self.l1_
        W1_end = w1_[1] * w1_[0]
        #self.w1_ = np.reshape(params[W1_start:W1_end], (self.l1_ , self.l2_))
        self.w1_ = np.reshape(params[W1_start:W1_end], w1_)
        #W2_end = W1_end + self.l2_ * self.l3_
        W2_end = W1_end + w2_[1] * w2_[0]
        self.w2_ = np.reshape(params[W1_end:W2_end], (self.l2_ , self.l3_))

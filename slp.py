import numpy as np

class SingleLayerPerceptron :
    def __init__(self, inUnits, eta=0.5) :
        self.inUnits = inUnits
        self.w_ = np.array(np.absolute(np.random.uniform(inUnits+1)))
        self.eta_ = eta
        
    def propagate(self, X) :
        X = self.addBias(X)
        result = np.dot(X, self.w_)
        return self.activation(result)
    
    def learn(self, X, result, target) :
        delta = (target - result) 
        self.w_ = self.w_ + np.array((self.eta_*delta*np.array(X)))
        
    def addBias(self, X) :
        return [1] + X
        
    def activation(self, val) :
        return int(val >= 0)
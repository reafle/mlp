import numpy as np
import math

# We have to normalize the data for the NN to perform well, otherwise we might not find an optimal solution at all
def normalize(data) :
    return data/np.amax(data, axis=0)

# Used for fast changing dataset
def getRotationMatrix(angle) :
    return np.matrix( ((math.cos(angle),-math.sin(angle)), (math.sin(angle), math.cos(angle))) )

def rotate2d(X, degrees):
    rad = degrees * np.pi / 180
    return np.array(X * np.matrix( ((math.cos(rad),-math.sin(rad)), (math.sin(rad), math.cos(rad))) ))


class Moon :
    def genCirclePoints(self, numSamples=100, noise=0, radius=10., threshold=0., xOffset=0., yOffset=0.) :
        points=[]
        for i in range(int((numSamples))) :
            r = np.random.uniform(threshold, (threshold+radius)*0.5)
            angle = np.random.uniform(low=0, high=2*np.pi);
            x = xOffset + r * np.sin(angle);
            y = yOffset + r * np.cos(angle);
            noiseX = xOffset + np.random.uniform(low=-radius, high=radius)
            noiseY = yOffset + np.random.uniform(low=-radius, high=radius)
            if noise > 0 and np.random.randint(100) < noise :
                x = noiseX
                y = noiseY

            points.append([x, y]);
        return np.array(points)
    
    def cut(self, points, up=True, cutoffY=1) :
        if up == True :
            return [X for X in points if X[1] > cutoffY]
        elif up == False :
            return [X for X in points if X[1] < cutoffY]
        else :
            assert "'Up' param has to be True/False"
    
    def genClass(self, numSamples=200, noise=0, radius=10., xOffset=0, threshold=5., up=True, cutoff=-1) :
        # generate the circle
        points = self.genCirclePoints(numSamples=numSamples, noise=noise, radius=radius, xOffset=xOffset, threshold=threshold)
        
        # cut the points
        points = self.cut(points, up=up, cutoffY=cutoff)
        
        return np.array(points)
        
    def genClasses(self, numSamples=200) :
        X = self.genClass(numSamples=numSamples, noise=0, radius=10., threshold=5., up=True, cutoff=-0.5)
        Y = self.genClass(numSamples=numSamples, noise=0, radius=10., xOffset=5, threshold=5., up=False, cutoff=0.5)
        return X, Y    
    
    def getFast(self, degrees=0, numSamples=150) :
        #print ("FAST : degrees %d, numSamples %d" % (degrees, numSamples) )
        
        points1 = self.genClass(numSamples=numSamples, noise=0, radius=10., xOffset=-5, threshold=5., up=True, cutoff=-1)
        points2 = self.genClass(numSamples=numSamples, noise=0, radius=10., xOffset=0, threshold=5., up=False, cutoff=1)
        if degrees > 0 :
            points1 = rotate2d(points1, degrees)
            points2 = rotate2d(points2, degrees)
        return points1, points2

    def getSlow(self, step=1, numSamples=150) :
        #print ("SLOW : Step %d, numSamples %d" % (step, numSamples) )
        r = step + 10 
        d = r - 5
        x = r / 2 
        return self.genClass(numSamples=numSamples, noise=0, radius=r, xOffset=0, threshold=d, up=True, cutoff=-1), self.genClass(numSamples=numSamples, noise=0, radius=r, xOffset=x, threshold=d, up=False, cutoff=1)
       
        
    def getNoise(self, step=0, numSamples=150) :
        #print ("NOISE : Step %d, numSamples %d" % (step, numSamples) )
        return self.genClass(numSamples=numSamples, noise=step, radius=10., xOffset=-5, threshold=5., up=True, cutoff=-1), self.genClass(numSamples=numSamples, noise=step, radius=10., xOffset=0, threshold=5., up=False, cutoff=1)
        
    
class Iris :
    def __init__ (self) :
        self.classMap = { 
            "Iris-setosa" : np.array([1,0,0]), 
            "Iris-versicolor" : np.array([0,1,0]), 
            "Iris-virginica" : np.array([0,0,1] )
        }
        self.classMap2d = { 
            "Iris-setosa" : np.array([1]), 
            "Iris-versicolor" : np.array([0])
        }
        self.traininigData = []
        self.traininigDataResults = []
        
        self.testingData = []
        self.testingDataResults = []
        
    def scanInputFile(self, filename, setType='4d') :
        with open(filename, 'r') as fHandle :
            data = []
            classes = []
            for line in fHandle.readlines() :
                elements = [item for item in line[:-1].split(',')]

                # flower data
                data.append([ float(item) for item in elements[:-1] ])

                # Class value
                classes.append(self.encode(elements[-1], setType))
                    
        data = normalize(data)
        classes = normalize(classes)
        return np.array(data), np.array(classes)
    
    def encode(self, className, setType) :
        if setType == '4d' :
            return self.classMap.get(className)
        elif setType == '2d' :
            return self.classMap2d.get(className)
        
    def decode(self, value, setType='4d') :
        if setType == '4d' :
            items = self.classMap.items()
        elif setType == '2d' :
            items = self.classMap2d.items()
            
        for item in items :
            if np.array_equal(item[1], value) :
                return item[0]    

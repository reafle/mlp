import scipy as sp
import numpy as np

class Trainer :
    def __init__(self, nn):
        self.nn = nn
        self.j_ = []
        
    def trackError(self, params):
        oldParams = self.nn.getParams()
        self.nn.setParams(params)
        self.j_.append(self.nn.cost(self.X, self.y))   
        self.nn.setParams(oldParams)
        
    #deprecate
    def costWrapper(self, params, X, y):
        oldParams = self.nn.getParams()
        
        self.nn.setParams(params)
        cost = self.nn.cost(X, y)
        grad = self.nn.computeGradients(X,y)
        
        self.nn.setParams(oldParams)
        
        return cost, grad
    
    def cost(self, params, X, y):
        #print("\nTrainer.Cost\n")
        oldParams = self.nn.getParams()
        
        self.nn.setParams(params)
        cost = self.nn.cost(X, y)
        self.nn.setParams(oldParams)
        
        return cost
    
    def gradient(self, params, X, y):
        #print("\nTrainer.Gradient\n")
        oldParams = self.nn.getParams()
        
        self.nn.setParams(params)
        grad = self.nn.computeGradients(X,y)
        
        self.nn.setParams(oldParams)
        
        return grad
            
    def train(self, X, y, epochs=100, eta=0.03, tolerance=0.1, method="BFGS", disp=True) :
        if self.nn.bias == True and X.shape[1] != self.nn.l1_:
            X = np.insert(X, obj=[0], values=[1], axis=1)
            
        self.X = X
        self.y = y

        options = { 'maxiter': epochs, 'disp' : disp, 'tolerance' : tolerance, 'epsilon' : eta}

        _res = None
        # thank you, scipy
        if method == 'BFGS' :
            _res = self.trainBfgs(X, y, options)
        elif method == 'L-BFGS-B' :
            _res = self.trainLbfgsb(X, y, options)
        #elif method == 'Newton-CG' :
            #_res = self.trainNewtonCg(X, y, options)
        elif method == 'CG' :
            _res = self.trainCg(X, y, options)
        elif method == 'Nelder-Mead' :
            _res = self.trainNelderMead(X, y, options)
        else :
            raise NameError('Method should be one of the: BFGS, L-BFGS-B, CG, Nelder-Mead')
            
        #self.nn.debug and print(_res)

        self.nn.setParams(_res)
        self.optimizationResults = _res
        
    def trainBfgs(self, X, y, options) :
        print('optimizing by BFGS')
        return sp.optimize.fmin_bfgs(f=self.cost, fprime=self.gradient, args=(X, y), x0=self.nn.getParams(), callback=self.trackError, gtol=options['tolerance'], epsilon=options['epsilon'], maxiter=options['maxiter'], disp=options['disp'])
    
    def trainLbfgsb(self, X, y, options) :
        print('optimizing by L-BFGS-B')
        options['disp'] = int(options['disp'])
        result = sp.optimize.fmin_l_bfgs_b(func=self.cost, fprime=self.gradient, args=(X, y), x0=self.nn.getParams(), approx_grad=True, callback=self.trackError, epsilon=options['epsilon'], pgtol=options['tolerance'], maxiter=options['maxiter'], disp=options['disp'])
        return result[0]
    
    #deprecated
    def trainNewtonCg(self, X, y, options) :
        #options['xtol'] = tolerance
        print('optimizing by Newton-CG')
        return sp.optimize.fmin_ncg(f=self.cost, fprime=self.gradient, fhess=self.hessian, args=(X, y), x0=self.nn.getParams(), callback=self.trackError, avextol=options['tolerance'], epsilon=options['epsilon'], maxiter=options['maxiter'],  disp=options['disp'])
    
    def trainCg(self, X, y, options) :
        return sp.optimize.fmin_cg(f=self.cost, fprime=self.gradient, args=(X, y), x0=self.nn.getParams(), callback=self.trackError, gtol=options['tolerance'], maxiter=options['maxiter'], disp=options['disp'], epsilon=options['epsilon'])
    
    def trainNelderMead(self, X, y, options) :
        return sp.optimize.fmin(func=self.cost, args=(X, y), x0=self.nn.getParams(), callback=self.trackError, xtol=options['tolerance'], maxiter=options['maxiter'], disp=options['disp'])        
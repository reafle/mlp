import numpy as np
# This class holds several activation functions

class Activation :
    def sigmoid(self, val) :
        try:
            return 1/(1 + np.exp(-val))
        except OverflowError:
            return float("inf")
    
    def sigmoidPrime(self, val) :
        try :
            return np.exp(-val)/((1 + np.exp(-val))**2)
        except OverflowError:
            return float("inf")
    
    def tanh(self,val):
        try:
            return np.tanh(val)
        except OverflowError:
            return float("inf")
    
    def tanh2(self, val) :
        return (np.exp(val)-np.exp(-val)) / (np.exp(val)+np.exp(-val))
    
    
    def tanhPrime(self, val) :
        try:
            return 1-np.tanh(val)**2
        except OverflowError:
            return float("inf")
        #return (2 / (np.exp(val)-np.exp(-val)))**2
    
    def linear(self,val) :
        return val
    
    def linearPrime(self, val) :
        return np.ones(val.shape)
    
    def sigmoidSpecial(self, val) :
        try:
            return np.multiply(1.7159, np.tanh(np.multiply((2/3), val)))
        except OverflowError:
            return float("inf")
    
    def sigmoidSpecialPrime(self, val) :
        try:
            return np.multiply(1.14393, (1/np.cosh(np.multiply(2, val/3))**2))
        except OverflowError:
            return float("inf")
        
    
    #for debugging
    def none(self, val) :
        return val
    
    def nonePrime(self, val) :
        return val
        